package models

import "gorm.io/gorm"

type Discount struct {
	gorm.Model
	Name            string
	Description     string
	DiscountPercent int
	Product         []Product
}

type DiscountInput struct {
	Name            string `binding:"required"`
	Description     string `binding:"required"`
	DiscountPercent int    `json:"discount_percent" binding:"required"`
}
