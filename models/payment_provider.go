package models

import "gorm.io/gorm"

type PaymentProvider struct {
	gorm.Model
	Name        string `gorm:"not null"`
	OrderDetail []OrderDetail
}

type PaymentProviderInput struct {
	Name string `binding:"required"`
}
