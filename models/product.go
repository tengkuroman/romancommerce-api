package models

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	Name, Description, ImageUrl    string
	Price                          int
	DiscountID, UserID, CategoryID uint
	OrderItem                      OrderItem
	CartItem                       CartItem
}

type ProductInput struct {
	Name        string `binding:"required"`
	Description string `binding:"required"`
	ImageUrl    string `json:"image_url" binding:"required"`
	Price       int    `binding:"required"`
	DiscountID  uint   `json:"discount_id"`
	CategoryID  uint   `json:"category_id" binding:"required"`
}
