package main

import (
	"log"
	"romancommerce-api/api/routes"
	"romancommerce-api/config"
	"romancommerce-api/docs"
	"romancommerce-api/utils"

	"github.com/joho/godotenv"
)

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @termsOfService http://swagger.io/terms/

func main() {
	// Load environment info
	environment := utils.GetEnv("ENVIRONMENT", "development")

	if environment == "development" {
		err := godotenv.Load()

		if err != nil {
			log.Fatal("Error loading .env file!")
		}
	}

	// Set swagger info
	docs.SwaggerInfo.Title = "Romancommerce API Documentation"
	docs.SwaggerInfo.Description = "API documentation for multiple level users."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = utils.GetEnv("SWAGGER_HOST", "localhost.com:8080")
	docs.SwaggerInfo.Schemes = []string{"https", "http"}

	// Connect database
	db := config.ConnectDatabase()
	databaseSQL, _ := db.DB()
	defer databaseSQL.Close()

	// Router
	r := routes.SetupRouter(db)
	r.Run()
}
