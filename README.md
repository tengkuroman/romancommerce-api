An e-commerce backend app built using Golang with framework Gin and Gorm ORM. 
- API documentation: https://romancommerce-api.herokuapp.com/swagger/index.html
- The database entity relationship diagram is shown below.
![image](https://gitlab.com/tengkuroman/romancommerce-api/-/raw/master/database_schema.png) 
