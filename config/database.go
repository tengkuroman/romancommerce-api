package config

import (
	"fmt"
	"os"
	"romancommerce-api/models"
	"romancommerce-api/utils"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDatabase() *gorm.DB {
	environment := utils.GetEnv("ENVIRONMENT", "development")

	if environment == "production" {
		username := os.Getenv("DATABASE_USERNAME")
		password := os.Getenv("DATABASE_PASSWORD")
		host := os.Getenv("DATABASE_HOST")
		port := os.Getenv("DATABASE_PORT")
		database := os.Getenv("DATABASE_NAME")

		dsn := "host=" + host + " user=" + username + " password=" + password + " dbname=" + database + " port=" + port + " sslmode=require"
		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		if err != nil {
			panic(err.Error())
		}

		db.AutoMigrate(
			&models.Category{},
			&models.Discount{},
			&models.PaymentProvider{},
			&models.User{},
			&models.Product{},
			&models.CartItem{},
			&models.OrderDetail{},
			&models.OrderItem{},
			&models.ShoppingSession{},
		)

		return db
	} else {
		const (
			username = "root"
			password = ""
			host     = "tcp(127.0.0.1:3306)"
			database = "db_romancommerce"
		)

		dsn := fmt.Sprintf("%v:%v@%v/%v?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, database)

		db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
		if err != nil {
			panic(err.Error())
		}

		return db
	}

}
