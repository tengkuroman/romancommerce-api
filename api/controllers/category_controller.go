package controllers

import (
	"net/http"
	"romancommerce-api/models"
	"romancommerce-api/utils"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// @Summary Get all product categories.
// @Description Get all product categories, including unsigned to product categories.
// @Tags Category
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /categories [get]
func GetAllCategories(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var categories []models.Category

	db.Find(&categories)

	c.JSON(http.StatusOK, gin.H{
		"data": categories,
	})
}

// @Summary Get product category by ID.
// @Description Get product category by category_id.
// @Tags Category
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /category/{category_id} [get]
// @Param category_id path string true "Required param."
func GetCategoryByID(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var category models.Category

	db.First(&category, c.Param("category_id"))

	c.JSON(http.StatusOK, gin.H{
		"data": category,
	})
}

// @Summary Post category (role: admin)
// @Description Post product category. Only admin can post category. Switch your role if you are not admin.
// @Tags Category
// @Param body body models.CategoryInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /category [post]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func PostCategory(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can create category!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var input models.CategoryInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	category := models.Category{
		Name:        input.Name,
		Description: input.Description,
	}

	db.Create(&category)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Category created successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Update product category (role: admin)
// @Description Update product category by category_id. Only admin can update category. Switch your role if you are not admin.
// @Tags Category
// @Param body body models.CategoryInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /category/{category_id} [patch]
// @Param category_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func UpdateCategory(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can update category!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var input models.CategoryInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var category models.Category

	if err := db.Where("id = ?", c.Param("category_id")).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Category data not found!"})
		return
	}

	db.Model(&category).Updates(input)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Category data changed successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Delete product category (role: admin)
// @Description Delete product category. Only admin can delete category. Switch your role if you are not admin.
// @Tags Category
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /category/{category_id} [delete]
// @Param category_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func DeleteCategory(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can delete category!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var category models.Discount

	if err := db.Where("id = ?", c.Param("category_id")).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Category data not found!"})
		return
	}

	db.Delete(&category)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Category deleted successfully!",
		"username": USER_DATA["username"],
	})
}
