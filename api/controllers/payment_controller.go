package controllers

import (
	"net/http"
	"romancommerce-api/models"
	"romancommerce-api/utils"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// @Summary Get payment providers.
// @Description Get payment providers. Authenticated user can access this.
// @Tags Payment Provider
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /payment [get]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func GetPaymentProviders(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var providers []models.PaymentProvider

	db.Find(&providers)

	c.JSON(http.StatusOK, gin.H{
		"data":     providers,
		"username": USER_DATA["username"],
	})
}

// @Summary Post payment provider (role: admin)
// @Description Post payment provider. Only admin can post it. Switch your role if you are not admin.
// @Tags Payment Provider
// @Param body body models.PaymentProviderInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /payment [post]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func PostPaymentProvider(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can create payment provider!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var input models.PaymentProviderInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	provider := models.PaymentProvider{
		Name: input.Name,
	}

	db.Create(&provider)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Payment provider created successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Update payment provider (role: admin)
// @Description Update payment provider. Only admin update it. Switch your role if you are not admin.
// @Tags Payment Provider
// @Param body body models.PaymentProviderInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /payment/{payment_provider_id} [patch]
// @Param payment_provider_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func UpdatePaymentProvider(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can update payment provider!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var input models.PaymentProviderInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var provider models.PaymentProvider

	if err := db.Where("id = ?", c.Param("payment_provider_id")).First(&provider).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Payment provider not found!"})
		return
	}

	db.Model(&provider).Updates(input)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Payment provider changed successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Delete payment provider (role: admin)
// @Description Delete payment provider. Only admin can delete it. Switch your role if you are not admin.
// @Tags Payment Provider
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /payment/{payment_provider_id} [delete]
// @Param payment_provider_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func DeletePaymentProvider(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can delete payment provider!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var provider models.PaymentProvider

	if err := db.Where("id = ?", c.Param("payment_provider_id")).First(&provider).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Payment provider not found!"})
		return
	}

	db.Delete(&provider)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Payment provider deleted successfully!",
		"username": USER_DATA["username"],
	})
}
