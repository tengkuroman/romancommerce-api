package controllers

import (
	"fmt"
	"net/http"
	"romancommerce-api/models"
	"romancommerce-api/utils"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// @Summary Get all products.
// @Description Get all products available in marketplace.
// @Tags Product
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /products [get]
func GetAllProducts(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product

	db.Find(&products)

	c.JSON(http.StatusOK, gin.H{
		"data": products,
	})
}

// @Summary Get product by ID.
// @Description Get specific product by product_id.
// @Tags Product
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /product/{product_id} [get]
// @Param product_id path int true "Param required."
func GetProductByID(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var product models.Product

	db.First(&product, c.Param("product_id"))

	c.JSON(http.StatusOK, gin.H{
		"data": product,
	})
}

// @Summary Get products by discount.
// @Description Get specific products by discount_id.
// @Tags Product
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /products/discount/{discount_id} [get]
// @Param discount_id path int true "Param required."
func GetProductsByDiscountID(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product

	db.Where("discount_id = ?", c.Param("discount_id")).Find(&products)

	c.JSON(http.StatusOK, gin.H{
		"data": products,
	})
}

// @Summary Get products from specific seller.
// @Description Get specific products by seller_id.
// @Tags Product
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /products/seller/{user_id} [get]
// @Param user_id path int true "Param required."
func GetProductsBySellerID(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product

	db.Where("user_id = ?", c.Param("user_id")).Find(&products)

	c.JSON(http.StatusOK, gin.H{
		"data": products,
	})
}

// @Summary Get products from specific category.
// @Description Get specific products by category_id.
// @Tags Product
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /products/category/{category_id} [get]
// @Param category_id path int true "Param required."
func GetProductsByCategoryID(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product

	db.Where("category_id = ?", c.Param("category_id")).Find(&products)

	c.JSON(http.StatusOK, gin.H{
		"data": products,
	})
}

// @Summary Post product (role: seller)
// @Description Post product to marketplace. Switch your role if you are not seller.
// @Tags Product
// @Param body body models.ProductInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /product [post]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func PostProduct(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "seller" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only sellers can create product!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var input models.ProductInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	UserID, err := strconv.ParseUint(fmt.Sprintf("%v", USER_DATA["user_id"]), 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Cannot parse user_id!"})
		return
	}

	product := models.Product{
		Name:        input.Name,
		Description: input.Description,
		ImageUrl:    input.ImageUrl,
		Price:       input.Price,
		DiscountID:  input.DiscountID,
		UserID:      uint(UserID),
		CategoryID:  input.CategoryID,
	}

	db.Create(&product)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Product created successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Update product (role: seller)
// @Description Update posted product by product_id. Seller can only update their own products. Switch your role if you are not seller.
// @Tags Product
// @Param body body models.ProductInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /product/{product_id} [patch]
// @Param product_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func UpdateProduct(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "seller" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can update product!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var product models.Product

	if err := db.Where("id = ?", c.Param("product_id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product not found!"})
		return
	}

	if product.UserID != USER_DATA["user_id"] {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "You can only update your own product!"})
		return
	}

	var input models.ProductInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db.Model(&product).Updates(input)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Product data changed successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Delete product (role: seller)
// @Description Delete posted product by product_id. Seller can only delete their own products. Switch your role if you are not seller.
// @Tags Product
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /product/{product_id} [delete]
// @Param product_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func DeleteProduct(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "seller" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only sellers can delete product!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var product models.Product

	if err := db.Where("id = ?", c.Param("product_id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Product not found!"})
		return
	}

	if product.UserID != USER_DATA["user_id"] {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "You can only delete your own product!"})
		return
	}

	db.Delete(&product)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Product deleted successfully!",
		"username": USER_DATA["username"],
	})
}
