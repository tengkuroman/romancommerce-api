package controllers

import (
	"fmt"
	"net/http"
	"romancommerce-api/models"
	"romancommerce-api/utils"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

// @Summary Register a user.
// @Description Registering a user from public access.
// @Tags User Auth
// @Param body body models.RegisterInput true "Body to register a user."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.RegisterInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := models.User{
		FirstName:   input.FirstName,
		LastName:    input.LastName,
		Username:    input.Username,
		Email:       input.Email,
		Password:    input.Password,
		Address:     input.Address,
		PhoneNumber: input.PhoneNumber,
		Role:        "user", // default role when registering
	}

	_, err := user.SaveUser(db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message":  "Registration success!",
		"username": user.Username,
	})
}

// @Summary Login as as user, seller, or admin.
// @Description Logging in to get JWT token to access certain API by roles.
// @Tags User Auth
// @Param body body models.LoginInput true "Body required to login."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	token, err := models.LoginCheck(input.Username, input.Password, db)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Username or password is incorrect!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message":  "Login success!",
		"username": input.Username,
		"token":    token,
	})
}

// @Summary Change user password.
// @Description Change user password for all roles.
// @Tags User Auth
// @Param body body models.ChangePasswordInput true "Body required to change password."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /change/password [patch]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func ChangePassword(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var input models.ChangePasswordInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User

	if err := db.Where("username = ?", c.Param("username")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User not found!"})
		return
	}

	if err := models.VerifyPassword(input.OldPassword, user.Password); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	newHashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Password hashing error!"})
		return
	}

	db.Model(&user).Update("password", newHashedPassword)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Password changed successfully!",
		"username": user.Username,
	})
}

// @Summary Change user details.
// @Description Change user detail: name, email, address, phone number.
// @Tags User Auth
// @Param body body models.ChangeUserDetailInput true "Body required to user detail(s)."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /change [patch]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func ChangeUserDetail(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var input models.ChangeUserDetailInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if err := db.Where("username = ?", USER_DATA["username"]).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User not found!"})
		return
	}

	db.Model(&user).Updates(input)

	c.JSON(http.StatusOK, gin.H{
		"message":  "User detail(s) changed successfully!",
		"username": user.Username,
	})
}

// @Summary Switch user role.
// @Description Change user role: user, seller, admin. Please re-login after switch.
// @Tags User Auth
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /switch/{role} [patch]
// @Param role path string true "Available roles: user, seller, admin"
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func SwitchUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var user models.User

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if err := db.Where("username = ?", USER_DATA["username"]).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User not found!"})
		return
	}

	newRole := c.Param("role")

	switch newRole {
	case "user":
	case "seller":
	case "admin":
	default:
		c.JSON(http.StatusBadRequest, gin.H{"error": "Role invalid!"})
		return
	}

	if newRole == user.Role {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Already in requested role!"})
		return
	}

	db.Model(&user).Update("role", newRole)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Role changed successfully!",
		"username": user.Username,
	})
}
