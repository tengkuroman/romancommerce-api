package controllers

import (
	"fmt"
	"net/http"
	"romancommerce-api/models"
	"romancommerce-api/utils"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/imdario/mergo"
	"gorm.io/gorm"
)

// @Summary Add a product to cart.
// @Description Add a product to cart.
// @Tags Shopping
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /cart [post]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func AddProductToCart(c *gin.Context) {
	// Check active shopping session by user_id
	//      if exist then add to current session, update total in session
	//      if not exist then create session and add the product, update total in session

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var session models.ShoppingSession
	var item models.CartItem

	if err := c.ShouldBindJSON(&item); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("user_id = ?", USER_DATA["user_id"]).Last(&session).Error; err != nil {
		UserID, err := strconv.ParseUint(fmt.Sprintf("%v", USER_DATA["user_id"]), 10, 32)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Cannot parse user_id!"})
			return
		}

		session.UserID = uint(UserID)
		db.Create(&session)
		db.Where("user_id = ?", USER_DATA["user_id"]).Last(&session)

		item.ShoppingSessionID = session.ID
		db.Create(&item)

		var product models.Product
		db.Where("product_id = ?", item.ProductID).First(&product)

		itemTotalPrice := product.Price * item.Quantity
		db.Model(&session).Update("total", itemTotalPrice)

		c.JSON(http.StatusOK, gin.H{
			"message":  "Product added to the cart!",
			"username": USER_DATA["user_id"],
		})

		return
	}

	item.ShoppingSessionID = session.ID
	db.Create(&item)

	var product models.Product
	db.Where("product_id = ?", item.ProductID).First(&product)

	totalPrice := session.Total + item.Quantity*product.Price
	db.Model(&session).Update("total", totalPrice)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Product added to the cart!",
		"username": USER_DATA["user_id"],
	})

	return
}

// @Summary Get all products from cart.
// @Description Get all products from cart. Data retrieved based on logged in user.
// @Tags Shopping
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /cart [get]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func GetCartItems(c *gin.Context) {
	// Check active shopping session by user_id
	//      If exist then get items by shopping session
	//		If not exist then return "no items added to the cart"

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var session models.ShoppingSession

	if err := db.Where("user_id = ?", USER_DATA["user_id"]).Last(&session).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "No items added to the cart!"})
		return
	}

	var items []models.CartItem

	if err := db.Where("session_id = ?", session.ID).Find(&items).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data":     items,
		"username": USER_DATA["user_id"],
	})

	return
}

// @Summary Update a product quantity in cart.
// @Description Update a product quantity in cart.
// @Tags Shopping
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /cart [patch]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func UpdateCartItem(c *gin.Context) {
	// Check active shopping session by user_id -> get the shopping session id
	//     If exist then check if in shopping session there is a product_id == update item's product_id
	//			If exist then merge the data with the update
	//			If not exist then return "please use add product method"

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var session models.ShoppingSession

	if err := db.Where("user_id = ?", USER_DATA["user_id"]).Last(&session).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Please use add product method!"})
		return
	}

	var inputItem models.CartItem

	if err := c.ShouldBindJSON(&inputItem); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var item models.CartItem

	if err := db.Where(&models.CartItem{ShoppingSessionID: session.ID, ProductID: inputItem.ProductID}).First(&item).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Please use add product method!"})
		return
	}

	mergo.Merge(&inputItem, item)
	db.Model(&item).Updates(inputItem)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Cart item updated successfully!",
		"username": USER_DATA["user_id"],
	})

	return
}

// @Summary Drop shopping cart.
// @Description Delete shopping session and all items in cart for current logged in user.
// @Tags Shopping
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /cart [delete]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func DropCart(c *gin.Context) {
	// Check active shopping session by user_id
	//		If exist then delete session and delete all cart items related to the session
	//		If not exist then return "no cart to be dropped"

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var session models.ShoppingSession

	if err := db.Where("user_id = ?", USER_DATA["user_id"]).Last(&session).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "No cart to be dropped!"})
		return
	}

	var item models.CartItem
	db.Where("session_id = ?", session.ID).Delete(&item)
	db.Delete(&session)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Cart dropped successfully!",
		"username": USER_DATA["user_id"],
	})
}

// @Summary Checkout shopping cart.
// @Description Bring all the items in cart to order.
// @Tags Shopping
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /cart/checkout [get]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func Checkout(c *gin.Context) {
	// Check active shopping session by user_id
	//		If exist then:
	//			Create order detail and get key, get cart items and store to order item with order detail key
	//			Delete session and delete all cart items related to the session
	//		If not exist then return "no cart to be checked out"

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var session models.ShoppingSession

	if err := db.Where("user_id = ?", USER_DATA["user_id"]).Last(&session).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "No cart to be checked out!"})
		return
	}

	var order models.OrderDetail
	order.Total = session.Total
	order.PaymentStatus = "unpaid" // default when checkout

	db.Create(&order)

	var cartItems []models.CartItem
	if err := db.Where("session_id = ?", session.ID).Find(&cartItems).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var orderItems []models.OrderItem
	for i := range cartItems {
		var orderItem models.OrderItem

		orderItem.Quantity = cartItems[i].Quantity
		orderItem.ProductID = cartItems[i].ProductID
		orderItem.OrderDetailID = order.ID

		orderItems = append(orderItems, orderItem)
	}

	db.Create(&orderItems)

	var modelCartItem models.CartItem
	db.Where("session_id = ?", session.ID).Delete(&modelCartItem)
	db.Delete(&session)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Cart checked out successfully!",
		"username": USER_DATA["user_id"],
	})
}
