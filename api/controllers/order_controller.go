package controllers

import (
	"fmt"
	"net/http"
	"romancommerce-api/models"
	"romancommerce-api/utils"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// @Summary Get all user's order.
// @Description Get all user's order. Order retrieved only that made by logged user.
// @Tags Order & Payment
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /orders [get]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func GetOrdersDetail(c *gin.Context) {
	// Get orders by user_id
	token := c.Query("token")
	fmt.Println(token)

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var orders models.OrderDetail
	db.Where("user_id", USER_DATA["user_id"]).Find(&orders)

	c.JSON(http.StatusOK, gin.H{
		"data":     orders,
		"username": USER_DATA["user_id"],
	})

	return
}

// @Summary Delete user's order.
// @Description Delete user's order. A user only can delete their own order.
// @Tags Order & Payment
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /order/delete/{order_detail_id} [delete]
// @Param order_detail_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func DeleteOrder(c *gin.Context) {
	// Check if an order exist based on param :order_detail_id
	// 		If order exist then check if order_detail.user.id == user_id
	//			OK: delete order_item where order_item.order_detail_id == order_detail.id, delete order_detail
	//			Not OK: Return message "You can only delete your order!"
	//		If order not exist then return "order detail not found"

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var order models.OrderDetail
	if err := db.Where("id = ?", c.Param("order_detail_id")).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Order detail not found!"})
		return
	}

	if order.UserID == USER_DATA["user_id"] {
		var item models.CartItem
		db.Where("order_detail_id = ?", order.ID).Delete(&item)
		db.Delete(&order)

		c.JSON(http.StatusOK, gin.H{
			"message":  "Order deleted successfully!",
			"username": USER_DATA["user_id"],
		})

		return
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "You can only delete your order!"})
		return
	}
}

// @Summary Select payment provider.
// @Description Select payment merchant after checkout (order created). A user can only pay their own order.
// @Tags Order & Payment
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /order/payment/{order_detail_id} [patch]
// @Param order_detail_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func SelectPaymentProvider(c *gin.Context) {
	// Check if an order exist based on param :order_detail_id
	// 		If order exist then check if order_detail.user.id == user_id
	//			OK: Update order_detail.payment_provider_id
	//			Not OK: Return message "You can only process payment of your order!"
	//		If order not exist then return "order detail not found"

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var order models.OrderDetail
	if err := db.Where("id = ?", c.Param("order_detail_id")).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Order detail not found!"})
		return
	}

	if order.UserID == USER_DATA["user_id"] {
		db.Model(&order).Update("payment_provider_id", c.Param("payment_provider_id"))

		c.JSON(http.StatusOK, gin.H{
			"message":  "Order deleted successfully!",
			"username": USER_DATA["user_id"],
		})

		return
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "You can only delete your order!"})
		return
	}
}

// @Summary Pay the order.
// @Description Pay the selected order. A user can only pay their own order.
// @Tags Order & Payment
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /order/payment/checkout/{order_detail_id} [patch]
// @Param order_detail_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func PayOrder(c *gin.Context) {
	// Check if an order exist based on param :order_detail_id
	// 		If order exist then check if order_detail.user.id == user_id
	//			OK: If order paid?
	//					OK: Update order_detail.payment_status
	//					Not OK: Return message "Order already paid!"
	//			Not OK: Return message "You can only pay your order!"
	//		If order not exist then return "order detail not found"

	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var order models.OrderDetail
	if err := db.Where("id = ?", c.Param("order_detail_id")).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Order detail not found!"})
		return
	}

	if order.UserID == USER_DATA["user_id"] {
		if order.PaymentStatus == "paid" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Order already paid!"})
			return
		} else {
			db.Model(&order).Update("payment_status", "paid")
		}

		c.JSON(http.StatusOK, gin.H{
			"message":  "Order deleted successfully!",
			"username": USER_DATA["user_id"],
		})

		return
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "You can only delete your order!"})
		return
	}
}
