package controllers

import (
	"net/http"
	"romancommerce-api/models"
	"romancommerce-api/utils"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// @Summary Get all discounts.
// @Description Get all discount data.
// @Tags Discount
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /discounts [get]
func GetAllDiscounts(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var discounts []models.Discount

	db.Find(&discounts)

	c.JSON(http.StatusOK, gin.H{
		"data": discounts,
	})
}

// @Summary Get discount by ID.
// @Description Get specific discount data by discount_id.
// @Tags Discount
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /discount/{discount_id} [get]
// @Param discount_id path int true "Param required."
func GetDiscountByID(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var discount models.Discount

	db.First(&discount, c.Param("discount_id"))

	c.JSON(http.StatusOK, gin.H{
		"data": discount,
	})
}

// @Summary Post discount data (role: admin)
// @Description Post discount data. Only admin post it. Switch your role if you are not admin.
// @Tags Discount
// @Param body body models.DiscountInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /discount [post]
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func PostDiscount(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can create discount!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var input models.DiscountInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	discount := models.Discount{
		Name:            input.Name,
		Description:     input.Description,
		DiscountPercent: input.DiscountPercent,
	}

	db.Create(&discount)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Discount created successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Update discount data (role: admin)
// @Description Update specific discount data by discount_id. Only admin can update it. Switch your role if you are not admin.
// @Tags Discount
// @Param body body models.DiscountInput true "Body required."
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /discount/{discount_id} [patch]
// @Param discount_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func UpdateDiscount(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can update discount!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var input models.DiscountInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var discount models.Discount

	if err := db.Where("id = ?", c.Param("discount_id")).First(&discount).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Discount data not found!"})
		return
	}

	db.Model(&discount).Updates(input)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Discount data changed successfully!",
		"username": USER_DATA["username"],
	})
}

// @Summary Deleete discount data (role: admin)
// @Description Delete specific discount data by discount_id. Only admin can delete it. Switch your role if you are not admin.
// @Tags Discount
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /discount/{discount_id} [delete]
// @Param discount_id path int true "Param required."
// @Param authorization header string true "How to input in Swagger: 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func DeleteDiscount(c *gin.Context) {
	USER_DATA, err := utils.ExtractPayloadData(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Extract payload data failed!"})
		return
	}

	if USER_DATA["role"] != "admin" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Only admins can delete discount!"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var discount models.Discount

	if err := db.Where("id = ?", c.Param("discount_id")).First(&discount).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Discount data not found!"})
		return
	}

	db.Delete(&discount)

	c.JSON(http.StatusOK, gin.H{
		"message":  "Discount deleted successfully!",
		"username": USER_DATA["username"],
	})
}
