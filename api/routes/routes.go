package routes

import (
	"romancommerce-api/api/controllers"
	"romancommerce-api/api/middlewares"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	// Set allow CORS
	r.Use(cors.Default())

	// Set context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// Swagger route
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Non auth route paths
	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)

	r.GET("/products", controllers.GetAllProducts)
	r.GET("/product/:product_id", controllers.GetProductByID)
	r.GET("/products/discount/:discount_id", controllers.GetDiscountByID)
	r.GET("/products/seller/:user_id", controllers.GetProductsBySellerID)
	r.GET("/products/category/:category_id", controllers.GetProductsByCategoryID)

	r.GET("/categories", controllers.GetAllCategories)
	r.GET("/category/:category_id", controllers.GetCategoryByID)

	r.GET("/discounts", controllers.GetAllDiscounts)
	r.GET("/discount/:discount_id", controllers.GetDiscountByID)

	// Router for authenticated access
	rAuth := r.Group("/")
	rAuth.Use(middlewares.AuthJWT())
	{
		// Auth role: user
		rAuth.PATCH("/change", controllers.ChangeUserDetail)
		rAuth.PATCH("/change/password/", controllers.ChangePassword)
		rAuth.PATCH("/switch/:role", controllers.SwitchUser)

		rAuth.POST("/cart", controllers.AddProductToCart)
		rAuth.GET("/cart", controllers.GetCartItems)
		rAuth.PATCH("/cart", controllers.UpdateCartItem)
		rAuth.DELETE("/cart", controllers.DropCart)
		rAuth.GET("/cart/checkout", controllers.Checkout)

		rAuth.GET("/orders", controllers.GetOrdersDetail)
		rAuth.DELETE("/order/delete/:order_detail_id", controllers.DeleteOrder)
		rAuth.PATCH("/order/payment/:order_detail_id/:payment_provider_id", controllers.SelectPaymentProvider)
		rAuth.PATCH("/order/payment/checkout/:order_detail_id", controllers.PayOrder)

		// Auth role: seller
		rAuth.POST("/product", controllers.PostProduct)
		rAuth.PATCH("/product/:product_id", controllers.UpdateProduct)
		rAuth.DELETE("/product/:product_id", controllers.DeleteProduct)

		// Auth role: admin
		rAuth.POST("/category", controllers.PostCategory)
		rAuth.PATCH("/category/:category_id", controllers.UpdateCategory)
		rAuth.DELETE("/category/:category_id", controllers.DeleteCategory)

		rAuth.POST("/discount", controllers.PostDiscount)
		rAuth.PATCH("/discount/:discount_id", controllers.UpdateDiscount)
		rAuth.DELETE("/discount/:discount_id", controllers.DeleteDiscount)

		rAuth.GET("/payment", controllers.GetPaymentProviders)
		rAuth.POST("/payment", controllers.PostPaymentProvider)
		rAuth.PATCH("/payment/:payment_provider_id", controllers.UpdatePaymentProvider)
		rAuth.DELETE("/payment/:payment_provider_id", controllers.DeletePaymentProvider)
	}

	return r
}
